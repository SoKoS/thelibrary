import 'sqlRequests.dart';

class Room {
  static create(
      {String location,
      String background,
      String floor,
      String topLeftModule,
      String topRightModule,
      String bottomLeftModule,
      String bottomRightModule,
      String topWall,
      String leftWall,
      String rightWall,
      String bottomWall,
      String topConnection,
      String leftConnection,
      String rightConnection,
      String bottomConnection}) {
    // TODO: CHANGE THE Floor to floor after i change the database
    return conn.query(
        'insert into rooms '
        '(location, background, floor, topLeftModule, topRightModule, bottomLeftModule, bottomRightModule, topWall, leftWall, rightWall, bottomWall, topConnection, leftConnection, rightConnection, bottomConnection)'
        ' values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        [
          location,
          background,
          floor,
          topLeftModule,
          topRightModule,
          bottomLeftModule,
          bottomRightModule,
          topWall,
          leftWall,
          rightWall,
          bottomWall,
          topConnection,
          leftConnection,
          rightConnection,
          bottomConnection
        ]);
  }
}
