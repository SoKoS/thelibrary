import 'sqlRequests.dart';

class Book {
  /// create a book and also the connection with the bookcase
  static create(
      {String location,
      String color = 'none',
      String text,
      String bookcaseLocation}) {
    return conn.query(
        'insert into books (location, color, text, bookcase) values (?, ?, ?, ?)',
        [bookcaseLocation + "," + location, color, text, bookcaseLocation]);
  }

  /// Get all books that are in a [bookcase] ex. ("0,1,0,3") x, y, z, module
  static getBooksInBookcase({String bookcase}) {
    return conn.query('select * from books where bookcase = "$bookcase"');
  }

  /// Gets a specific book in a [location] ex. ("0,1,0,3,4") x, y, z, module, index
  static getBook({String location}) {
    return conn.query('select * from books where location = "$location"');
  }
}
