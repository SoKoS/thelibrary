import 'package:mysql1/mysql1.dart';

MySqlConnection conn;

Future initSqlRequests() async {
  conn = await MySqlConnection.connect(ConnectionSettings(
      host: 'localhost', port: 3306, user: 'testkos', db: 'TheLibrary'));
}
