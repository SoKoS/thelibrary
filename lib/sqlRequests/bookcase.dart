import 'sqlRequests.dart';

class Bookcase {
  ///Documentation example
  ///
  ///[location] test
  static create({String roomLocation, String location}) {
    return conn.query('insert into bookcases (location) values (?)',
        [roomLocation + "," + location]);
  }
}
