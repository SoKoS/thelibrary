import 'dart:io';
import 'dart:async';

import 'package:mysql1/mysql1.dart';

import 'sqlRequests/book.dart';
import 'sqlRequests/bookcase.dart';
import 'sqlRequests/sqlRequests.dart';
import 'sqlRequests/room.dart';

Future main() async {
  await initSqlRequests();

  // await Room.create(location: "1,0,0", background: "test");
  // await Bookcase.create(roomLocation: "0,1,0", location: "3");
  // await Book.create(
  //     bookcaseLocation: '0,1,0,3',
  //     color: 'red',
  //     text: 'This is a test book!',
  //     location: '12');

  Results result = await conn.query('select * from rooms');
  print('test=${result}');

  Results result2 =
      await conn.query('select * from rooms where location = "0,0,0"');
  print('\n\ntest=${result2}');

  Results bookcases = await conn.query('select * from bookcases');
  print('\n\nbookcases=${bookcases}');

  Results books = await Book.getBook(location: "0,1,0,3,12");
  print('\nbook=${books}');

  Results allBooksInABookcase =
      await Book.getBooksInBookcase(bookcase: "0,1,0,3");
  print('\nbooks in bookcase=${allBooksInABookcase}');

  // Query the database using a parameterized query
  // var results = await conn
  //     .query('select name, email from users where id = ?', [result.insertId]);
  // for (var row in results) {
  //   print('Name: ${row[0]}, email: ${row[1]}');
  // }

  // Finally, close the connection
  await conn.close();
}
